package com.shazam;

/**
 * Created by jenzz on 25/11/14.
 */
final class Modules {

	private Modules() {
		// no instances
	}

	static Object[] list(ShazamApp app) {
		return new Object[] {new ShazamModule(app)};
	}

}
