package com.shazam.events;

import retrofit.RetrofitError;

/**
 * Created by jenzz on 25/11/14.
 */
public class TagsErrorEvent extends RetrofitErrorEvent {

	public TagsErrorEvent(RetrofitError error) {
		super(error);
	}
}
