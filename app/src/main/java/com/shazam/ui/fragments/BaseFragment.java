package com.shazam.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.shazam.ShazamApp;

import butterknife.ButterKnife;

/**
 * Created by jenzz on 25/11/14.
 */
public abstract class BaseFragment extends Fragment {

	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		ShazamApp.get(this).inject(this);
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.reset(this);
	}
}
