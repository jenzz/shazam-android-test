package com.shazam.ui.fragments.com.shazam.ui.activities;

import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;

import com.shazam.ui.activities.WebViewActivity;
import com.shazam.ui.fragments.TagsFragment;

/**
 * Created by jenzz on 25/11/14.
 */
public class MainActivityInstrumentationTest extends ActivityInstrumentationTestCase2<WebViewActivity> {

	private TagsFragment tagsFragment;

	public MainActivityInstrumentationTest(Class<WebViewActivity> activityClass) {
		super(activityClass);
	}

	protected void setUp() throws Exception {
		super.setUp();
		tagsFragment = waitForFragment(TagsFragment.TAG, 5000);
	}

	private <T extends Fragment> T waitForFragment(String tag, int timeout) {
		long endTime = SystemClock.uptimeMillis() + timeout;
		while (SystemClock.uptimeMillis() <= endTime) {
			Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(tag);
			if (fragment != null) {
				return (T) fragment;
			}
		}
		return null;
	}

}
