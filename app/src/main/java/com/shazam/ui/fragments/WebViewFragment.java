package com.shazam.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by jenzz on 25/11/14.
 */
public class WebViewFragment extends Fragment {

	public static final String TAG = WebViewFragment.class.getSimpleName();

	private static final String BUNDLE_URL = "url";

	private WebView webView;
	private boolean isWebViewAvailable;

	/**
	 * Called to instantiate the view. Creates and returns the WebView.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (webView != null) {
			webView.destroy();
		}
		webView = new WebView(getActivity());
		webView.setWebViewClient(new WebViewClient());
		isWebViewAvailable = true;
		return webView;
	}

	@Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle bundle = getArguments();
		if (bundle != null) {
			String url = bundle.getString(BUNDLE_URL);
			if (!TextUtils.isEmpty(url)) {
				webView.loadUrl(url);
			}
		}
	}

	/**
	 * Called when the fragment is visible to the user and actively running. Resumes the WebView.
	 */
	@Override
	public void onPause() {
		super.onPause();
		webView.onPause();
	}

	/**
	 * Called when the fragment is no longer resumed. Pauses the WebView.
	 */
	@Override
	public void onResume() {
		webView.onResume();
		super.onResume();
	}

	/**
	 * Called when the WebView has been detached from the fragment.
	 * The WebView is no longer available after this time.
	 */
	@Override
	public void onDestroyView() {
		isWebViewAvailable = false;
		super.onDestroyView();
	}

	/**
	 * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
	 */
	@Override
	public void onDestroy() {
		if (webView != null) {
			webView.destroy();
			webView = null;
		}
		super.onDestroy();
	}

	/**
	 * Gets the WebView.
	 */
	public WebView getWebView() {
		return isWebViewAvailable ? webView : null;
	}

	public static WebViewFragment newInstance(String url) {
		WebViewFragment webViewFragment = new WebViewFragment();
		Bundle bundle = new Bundle();
		bundle.putString(BUNDLE_URL, url);
		webViewFragment.setArguments(bundle);
		return webViewFragment;
	}

}
