package com.shazam;

import com.shazam.sdk.ShazamClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


import static org.mockito.Mockito.mock;

/**
 * Created by jenzz on 25/11/14.
 */
@Module(
		includes = ShazamModule.class,
		overrides = true,
		library = true)
public final class TestShazamModule {

	@Provides @Singleton ShazamClient provideShazamClient() { return mock(ShazamClient.class); }

}
