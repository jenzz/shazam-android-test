package com.shazam.events;

import com.shazam.sdk.model.Tag;

import java.util.List;

/**
 * Created by jenzz on 25/11/14.
 */
public class TagsLoadedEvent {

	private final List<Tag> tags;

	public TagsLoadedEvent(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Tag> getTags() {
		return tags;
	}
}
