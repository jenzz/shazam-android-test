package com.shazam.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shazam.R;
import com.shazam.api.TagsManager;
import com.shazam.events.TagsErrorEvent;
import com.shazam.events.TagsLoadedEvent;
import com.shazam.ui.adapters.TagsAdapter;
import com.shazam.ui.shared.DividerItemDecoration;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jenzz on 25/11/14.
 */
public class TagsFragment extends BaseFragment {

	public static String TAG = TagsFragment.class.getSimpleName();

	@InjectView(R.id.list) RecyclerView list;
	@InjectView(R.id.loading_spinner) View loadingSpinner;

	@Inject Bus bus;
	@Inject TagsManager tagsManager;

	private TagsAdapter adapter;

	@Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
									   @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_tags, container, false);
		ButterKnife.inject(this, view);

		adapter = new TagsAdapter(getActivity());

		int columnCount = getResources().getInteger(R.integer.column_count);
		list.setLayoutManager(new GridLayoutManager(getActivity(), columnCount));
		list.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
		list.setHasFixedSize(true); // performance boost!
		list.setAdapter(adapter);

		return view;
	}

	@Override public void onResume() {
		super.onResume();
		bus.register(this);
		tagsManager.getTags();
	}

	@Override public void onPause() {
		super.onPause();
		bus.unregister(this);
	}

	@Subscribe
	public void onEvent(TagsLoadedEvent e) {
		loadingSpinner.setVisibility(View.GONE);
		adapter.setTags(e.getTags());
	}

	@Subscribe
	public void onEvent(TagsErrorEvent e) {
		loadingSpinner.setVisibility(View.GONE);
		Toast.makeText(getActivity(), "Oops: " + e.getRetrofitError().getMessage(), Toast.LENGTH_SHORT).show();
	}

	public TagsAdapter getAdapter() { return adapter; }
}
