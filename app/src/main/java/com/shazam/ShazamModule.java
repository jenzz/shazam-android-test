package com.shazam;

import android.app.Application;
import android.content.res.Resources;

import com.shazam.sdk.ShazamClient;
import com.shazam.ui.activities.MainActivity;
import com.shazam.ui.adapters.TagsAdapter;
import com.shazam.ui.fragments.TagsFragment;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jenzz on 25/11/14.
 */
@Module(
		injects = {ShazamApp.class, MainActivity.class, TagsFragment.class, TagsAdapter.class},
		library = true)
public final class ShazamModule {

	private final ShazamApp app;

	public ShazamModule(ShazamApp app) {
		this.app = app;
	}

	@Provides @Singleton Application provideApplication() {
		return app;
	}

	@Provides @Singleton Resources provideResources() {
		return app.getResources();
	}

	@Provides @Singleton Bus provideBus() {
		return new Bus();
	}

	@Provides @Singleton ShazamClient provideShazamClient() { return ShazamClient.getInstance(); }

	@Provides @Singleton Picasso providePicasso() {
		return Picasso.with(app);
	}

}
