package com.shazam.ui.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.shazam.R;
import com.shazam.events.TagClickEvent;
import com.shazam.ui.fragments.TagsFragment;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;


public class MainActivity extends BaseActivity {

	@Inject Bus bus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
									   .add(R.id.container, new TagsFragment(), TagsFragment.TAG)
									   .commit();
		}
	}

	@Override protected void onResume() {
		super.onResume();
		bus.register(this);
	}

	@Override protected void onPause() {
		super.onPause();
		bus.unregister(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			Toast.makeText(this, R.string.action_settings, Toast.LENGTH_SHORT).show();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Subscribe
	public void onEvent(TagClickEvent e) {
		WebViewActivity.startActivity(this, e.getTrackArtist(), e.getTrackName(), e.getLink());
	}

}
