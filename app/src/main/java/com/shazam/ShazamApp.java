package com.shazam;

import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.shazam.events.RetrofitErrorEvent;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by jenzz on 25/11/14.
 */
public class ShazamApp extends Application {

	private ObjectGraph objectGraph;

	@Inject Bus bus;

	@Override public void onCreate() {
		super.onCreate();
		buildObjectGraphAndInject();
		bus.register(this);
	}

	private void buildObjectGraphAndInject() {
		objectGraph = ObjectGraph.create(Modules.list(this));
		objectGraph.inject(this);
	}

	public static ShazamApp get(Context context) {
		return (ShazamApp) context.getApplicationContext();
	}

	public static ShazamApp get(Fragment fragment) {
		return get(fragment.getActivity());
	}

	public void inject(Object o) {
		objectGraph.inject(o);
	}

	@Subscribe
	public void onEvent(RetrofitErrorEvent e) {
		// Subscribing to the base retrofit error event
		// Reporting errors & exceptions e.g. via Crashlytics
	}
}
