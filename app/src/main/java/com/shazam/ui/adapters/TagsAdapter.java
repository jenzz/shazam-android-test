package com.shazam.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shazam.R;
import com.shazam.ShazamApp;
import com.shazam.events.TagClickEvent;
import com.shazam.sdk.model.Tag;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jenzz on 25/11/14.
 */
public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {

	private final List<Tag> tags = new ArrayList<>();

	@Inject Bus bus;
	@Inject Picasso picasso;

	public TagsAdapter(Context context) {
		ShazamApp.get(context).inject(this);
	}

	public void setTags(List<Tag> tags) {
		this.tags.clear();
		this.tags.addAll(tags);
		notifyDataSetChanged();
	}

	@Override public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_tag, viewGroup, false);
		View.OnClickListener onClickListener = new View.OnClickListener() {
			@Override public void onClick(View v) {
				bus.post(new TagClickEvent(tags.get(i)));
			}
		};
		return new ViewHolder(view, onClickListener);
	}

	@Override public void onBindViewHolder(ViewHolder viewHolder, int i) {
		Tag tag = tags.get(i);

		viewHolder.artist.setText(tag.getTrackArtist());
		viewHolder.track.setText(tag.getTrackName());
		picasso.load(R.drawable.rss_icon).into(viewHolder.image);
	}

	@Override public int getItemCount() {
		return tags.size();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		@InjectView(R.id.image) ImageView image;
		@InjectView(R.id.artist) TextView artist;
		@InjectView(R.id.track) TextView track;

		ViewHolder(View itemView, View.OnClickListener onClickListener) {
			super(itemView);
			itemView.setOnClickListener(onClickListener);
			ButterKnife.inject(this, itemView);
		}
	}
}
