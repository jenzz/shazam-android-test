package com.shazam.events;

import com.shazam.sdk.model.Tag;

/**
 * Created by jenzz on 25/11/14.
 */
public class TagClickEvent {

	private Tag tag;

	public TagClickEvent(Tag tag) {
		this.tag = tag;
	}

	public String getTrackArtist() {
		return tag != null ? tag.getTrackArtist() : null;
	}

	public String getTrackName() {
		return tag != null ? tag.getTrackName() : null;
	}

	public String getLink() {
		return tag != null ? tag.getLink() : null;
	}
}
