package com.shazam.sdk;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.SimpleXMLConverter;

public final class ShazamClient {

	private static volatile ShazamClient client;

	private RestAdapter restAdapter;

	private TagsService tagsService;

	private ShazamClient() {
		OkHttpClient httpClient = new OkHttpClient();
		httpClient.setReadTimeout(10, TimeUnit.SECONDS);

		restAdapter = new RestAdapter.Builder().setEndpoint("http://www.shazam.com/music/web")
											   .setClient(new OkClient(httpClient))
											   .setConverter(new SimpleXMLConverter())
											   .build();
	}

	// use double-checked locking for thread safety
	public static ShazamClient getInstance() {
		if (client == null) {
			synchronized (ShazamClient.class) {
				if (client == null) client = new ShazamClient();
			}
		}
		return client;
	}

	// lazily init service
	public TagsService getTagsService() {
		if (tagsService == null) {
			tagsService = restAdapter.create(TagsService.class);
		}
		return tagsService;
	}
}
