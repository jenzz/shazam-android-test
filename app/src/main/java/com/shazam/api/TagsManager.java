package com.shazam.api;

import com.shazam.events.TagsErrorEvent;
import com.shazam.events.TagsLoadedEvent;
import com.shazam.sdk.ShazamClient;
import com.shazam.sdk.model.TagsResponse;
import com.squareup.otto.Bus;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jenzz on 25/11/14.
 */
@Singleton
public final class TagsManager {

	private final ShazamClient client;
	private final Bus bus;

	@Inject
	public TagsManager(ShazamClient client, Bus bus) {
		this.client = client;
		this.bus = bus;
	}

	public void getTags() {
		client.getTagsService().getTags(new Callback<TagsResponse>() {
			@Override public void success(TagsResponse tagsResponse, Response response) {
				bus.post(new TagsLoadedEvent(tagsResponse.getTags()));
			}

			@Override public void failure(RetrofitError error) {
				bus.post(new TagsErrorEvent(error));
			}
		});
	}
}
