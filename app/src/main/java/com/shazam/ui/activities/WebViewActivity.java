package com.shazam.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.shazam.R;
import com.shazam.ui.fragments.WebViewFragment;

/**
 * Created by jenzz on 25/11/14.
 */
public class WebViewActivity extends ActionBarActivity {

	private static final String EXTRAS_TITLE = "title";
	private static final String EXTRAS_SUBTITLE = "subtitle";
	private static final String EXTRAS_URL = "url";

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);

		String title = getIntent().getStringExtra(EXTRAS_TITLE);
		String subtitle = getIntent().getStringExtra(EXTRAS_SUBTITLE);

		getSupportActionBar().setTitle(title);
		getSupportActionBar().setSubtitle(subtitle);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState == null) {
			String url = getIntent().getStringExtra(EXTRAS_URL);
			getSupportFragmentManager().beginTransaction()
									   .add(R.id.container, WebViewFragment.newInstance(url), WebViewFragment.TAG)
									   .commit();
		}
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static Intent getIntent(Context context, String title, String subtitle, String url) {
		Intent intent = new Intent(context, WebViewActivity.class);
		intent.putExtra(EXTRAS_TITLE, title);
		intent.putExtra(EXTRAS_SUBTITLE, subtitle);
		intent.putExtra(EXTRAS_URL, url);
		return intent;
	}

	public static void startActivity(Context context, String title, String subtitle, String url) {
		context.startActivity(getIntent(context, title, subtitle, url));
	}
}
