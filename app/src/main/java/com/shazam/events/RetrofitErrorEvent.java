package com.shazam.events;

import retrofit.RetrofitError;

/**
 * Created by jenzz on 25/11/14.
 */
public abstract class RetrofitErrorEvent {

	private final RetrofitError error;

	public RetrofitErrorEvent(RetrofitError error) {
		this.error = error;
	}

	public RetrofitError getRetrofitError() {
		return error;
	}
}
