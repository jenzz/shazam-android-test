package com.shazam.sdk;

import com.shazam.sdk.model.TagsResponse;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jenzz on 25/11/14.
 */
public interface TagsService {

	@GET("/taglistrss?mode=xml&userName=shazam")
	void getTags(Callback<TagsResponse> callback);
}
