package com.shazam.sdk.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by jenzz on 25/11/14.
 */
@Root(strict = false)
public class TagsResponse {

	@Path("channel")
	@ElementList(entry = "item", inline = true)
	List<Tag> tags;

	public List<Tag> getTags() {
		return tags;
	}
}
