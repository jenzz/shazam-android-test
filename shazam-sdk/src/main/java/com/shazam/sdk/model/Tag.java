package com.shazam.sdk.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by jenzz on 25/11/14.
 */
@Root(strict = false)
public class Tag {

	@Element(name = "trackArtist")
	private String trackArtist;

	@Element(name = "trackName")
	private String trackName;

	@Element(name = "link")
	private String link;

	public String getTrackArtist() {
		return trackArtist;
	}

	public String getTrackName() {
		return trackName;
	}

	public String getLink() {
		return link;
	}
}
