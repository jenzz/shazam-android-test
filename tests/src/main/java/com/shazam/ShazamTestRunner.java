package com.shazam;

/**
 * Created by jenzz on 25/11/14.
 */

import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.res.Fs;

public class ShazamTestRunner extends RobolectricTestRunner {

	private static final int MAX_SDK_SUPPORTED_BY_ROBOLECTRIC = 18;

	/**
	 * Call this constructor to specify the location of resources and
	 * AndroidManifest.xml.
	 */
	public ShazamTestRunner(@SuppressWarnings("rawtypes") Class testClass) throws InitializationError {
		super(testClass);
	}

	/**
	 * Robolectric doesn't support Beamly's target SDK version 19 yet,
	 * so we need to create a new manifest based on our implementation,
	 * but return API Level 18 instead.
	 */
	@Override
	protected AndroidManifest getAppManifest(Config config) {
		String app = ".";
		String manifest = app + "/AndroidManifest.xml";
		String res = app + "/res";
		String assets = app + "/assets";
		AndroidManifest androidManifest =
				new AndroidManifest(Fs.fileFromPath(manifest), Fs.fileFromPath(res), Fs.fileFromPath(assets)) {

					@Override
					public int getTargetSdkVersion() {
						return MAX_SDK_SUPPORTED_BY_ROBOLECTRIC;
					}
				};
		return androidManifest;
	}

}
