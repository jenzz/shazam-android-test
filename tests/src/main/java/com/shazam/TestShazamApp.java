package com.shazam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jenzz on 25/11/14.
 */
public class TestShazamApp extends ShazamApp {

	List<Object> getModules() {
		List<Object> modules = new ArrayList<>();
		modules.add(new TestShazamModule());
		return modules;
	}
}
