package com.shazam.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.shazam.ShazamApp;

/**
 * Created by jenzz on 25/11/14.
 */
public abstract class BaseActivity extends ActionBarActivity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ShazamApp.get(this).inject(this);
	}
}
